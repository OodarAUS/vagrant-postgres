#!/bin/bash

# Include functions for postgres install
source /vagrant/postgres.sh

cat >> ~vagrant/.bashrc << EOL
source /vagrant/postgres.sh
EOL

install_postgres