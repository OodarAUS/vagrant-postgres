# Vagrant-Postgres #

This is a simple vagrant box to quickly spin up a Postgres database

## Getting Started ##

A simple 'vagrant up' will result in a Ubuntu Cloud box with a Postgres 9.4 server installed.

## Customisation ##
### PG Version ###
By default this script will install Postgres 9.4.
If you require a different version, then edit bootstrap.sh and specify the version as an argument to the "install_postgres" line.

e.g. to install Postgres 9.1

```
#!bash
install_postgres 9.1
```

### Host mapped port ###

By default this script will map the guest postgres port 5432 to 15432 on your host.
Edit the Vagrantfile to change this port.

Postgres will be 

## Configuration ##
Configuration options are provided using some provided shell functions.

Access your vagrant box with 'vagrant ssh' to do the following tasks

### Adding a user ###

To add a user simply enter:
create_user username password

e.g.

```
#!bash
create_user justin password123
```

### Creating a database ###

To create a database simply enter:
create_database database_name

e.g.

```
#!bash
create_user stock
```

### Grant privileges ###

To grant privileges, enter:
grant_privileges database user

e.g.

```
#!bash
grant_privileges stock justin
```