#!/bin/bash

## Execute an SQL statement on a particular database
## e.g.  execute_sql my_db "SELECT * from TABLE"
function execute_sql {
    sudo -u postgres psql $1 -c "$2"
}

## Install postgres. Version number should be passed in otherwise defaults to 9.4
## e.g.  install_postgres 9.2
function install_postgres {
    if [ "$#" -eq 1 ]
    then
        PG_VERSION=$1
    else
        PG_VERSION=9.4
    fi

    export DEBIAN_FRONTEND=noninteractive

    PG_REPO_APT_SOURCE=/etc/apt/sources.list.d/pgdg.list
    if [ ! -f "$PG_REPO_APT_SOURCE" ]
    then
      # Add PG apt repo:
      echo "deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main" > "$PG_REPO_APT_SOURCE"

      # Add PGDG repo key:
      wget --quiet -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | apt-key add -
    fi
    apt-get update
    apt-get -y install "postgresql-$PG_VERSION" "postgresql-contrib-$PG_VERSION" "postgresql-server-dev-$PG_VERSION"

    PG_CONF="/etc/postgresql/$PG_VERSION/main/postgresql.conf"
    PG_HBA="/etc/postgresql/$PG_VERSION/main/pg_hba.conf"
    PG_DIR="/var/lib/postgresql/$PG_VERSION/main"

    # Edit postgresql.conf to change listen address to '*':
    sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" "$PG_CONF"

    # Append to pg_hba.conf to add password auth:
    echo "host    all             all             all                     md5" >> "$PG_HBA"

    # Explicitly set default client_encoding
    echo "client_encoding = utf8" >> "$PG_CONF"

    # Restart so that all new config is loaded:
    service postgresql restart
}

## Create a database
## e.g. create_database my_db owner_name
## Note: Owner must exist prior to creating database
function create_database {
    DB=$1
    OWNER=$2
    execute_sql "" "CREATE DATABASE $DB WITH OWNER=$OWNER LC_COLLATE='en_US.utf8' LC_CTYPE='en_US.utf8' ENCODING='UTF8' TEMPLATE=template0;"
}

## Create a user
## e.g. create_user username password
function create_user {
    USER=$1
    PASS=$2
    # Create new user
    execute_sql "" "CREATE USER $USER WITH PASSWORD '$PASS'"
}

## Grant table and sequences privileges to the public schema for a given user
## e.g. grant_privileges my_db username
function grant_privileges {
    DB=$1
    USER=$2
    if [ "$#" -eq 3 ]
    then
        SCHEMA=$3
    else
        SCHEMA=public
    fi
   execute_sql $DB "GRANT CREATE ON DATABASE $DB TO $USER";
   execute_sql $DB "GRANT ALL PRIVILEGES ON SCHEMA $SCHEMA TO $USER;"
   execute_sql $DB "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA $SCHEMA TO $USER;"
   execute_sql $DB "GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA $SCHEMA TO $USER;"
}
